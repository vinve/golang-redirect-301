package main

import (
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	// Redirect to REDIRECT_DOMAIN, if set.
	domain := "example.com"
	if v, ok := os.LookupEnv("REDIRECT_DOMAIN"); ok {
		domain = v
	}

	// Configure our redirect server.
	s := &http.Server{
		Addr:         ":8080",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,

		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Connection", "close")
			url := "https://" + domain + r.URL.String()
			http.Redirect(w, r, url, http.StatusMovedPermanently)
		}),
	}

	// Start the server.
	log.Println("Starting redirect server...")
	log.Fatal(s.ListenAndServe())
}
